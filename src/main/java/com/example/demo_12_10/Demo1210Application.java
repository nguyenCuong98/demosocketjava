package com.example.demo_12_10;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo1210Application {

    public static void main(String[] args) {
        SpringApplication.run(Demo1210Application.class, args);
    }

}
