package com.example.demo_12_10.controller;

import com.example.demo_12_10.module.in.userIn;
import com.example.demo_12_10.service.TestService;
import com.example.demo_12_10.service.userService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@CrossOrigin("*")
@RequestMapping("/user")
public class userController {
    @Autowired
    private userService userService;
    @GetMapping("")
    public ResponseEntity<?> getAllData(){
        return new ResponseEntity<>(userService.GetAllData(), HttpStatus.OK);
    }
    @PostMapping("")
    public ResponseEntity<?> addData(@RequestBody userIn user){
        return new ResponseEntity<>(userService.AddData(user),HttpStatus.OK);
    }
   @PutMapping("/{Id}")
   public ResponseEntity<?> updateData(@PathVariable Integer Id,@RequestBody userIn userIn){
        return new ResponseEntity<>(userService.UpdateData(Id, userIn),HttpStatus.OK);
   }
   @DeleteMapping("/{Id}")
    public  ResponseEntity<?> deleteData(@PathVariable Integer Id){
        return new ResponseEntity<>(userService.DeleteData(Id),HttpStatus.OK);
   }
}
