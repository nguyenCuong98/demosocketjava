package com.example.demo_12_10.controller;

import com.example.demo_12_10.module.entity.CategoryEntity;
import com.example.demo_12_10.reponsitory.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@CrossOrigin("*")
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryRepository categoryRepository;

    @GetMapping("")
    public ResponseEntity<?> getCategory(){
       List<CategoryEntity> list = categoryRepository.findAll();
       return new ResponseEntity<>(list, HttpStatus.OK);
    }
}
