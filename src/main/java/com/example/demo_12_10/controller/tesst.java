package com.example.demo_12_10.controller;

import com.example.demo_12_10.module.in.userIn;
import com.example.demo_12_10.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@CrossOrigin
@RequestMapping("/test")
public class tesst {
    @Autowired
    private TestService testService;
    @GetMapping("")
    public ResponseEntity<?> test() {
        return new ResponseEntity<>(testService.get(), HttpStatus.OK);
    }

//    @PostMapping("")
//    public ResponseEntity<?>postAll(@RequestBody userIn userIn){
//        return new ResponseEntity<>(testService.postAll(userIn), HttpStatus.OK);
//    }
}
