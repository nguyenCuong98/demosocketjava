package com.example.demo_12_10.mapper;

import com.example.demo_12_10.module.dto.userDto;
import com.example.demo_12_10.module.entity.userEntity;
import com.example.demo_12_10.module.in.userIn;


public class userMapper {

        public static userEntity map(userIn userIn){
            userEntity userEntity =new userEntity();
            userEntity.setUserName(userIn.getUserName());
            if(userIn.getStatus()==null){
                userEntity.setStatus(true);
            }else{
                userEntity.setStatus(userIn.getStatus());
            }
            if(userIn.getPassWord()==null){
                userEntity.setPassWord("admin");
            }else {
                userEntity.setPassWord(userIn.getPassWord());
            }

            userEntity.setDateOfBirth(userIn.getDateOfBirth());
            return userEntity;

        }
     public static userDto mapDto(userEntity userEntity){
        userDto userDto =new userDto();
        userDto.setId(userEntity.getId());
        userDto.setUserName(userEntity.getUserName());
        userDto.setStatus(userEntity.getStatus());
        userDto.setDateOfBirth(userEntity.getDateOfBirth());
        userDto.setPassWord(userEntity.getPassWord());
        userDto.setCreatedDate(userEntity.getCreatedDate());
        return userDto;
     }
}
