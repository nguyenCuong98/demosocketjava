package com.example.demo_12_10.service;

import com.example.demo_12_10.module.bo.respont;
import com.example.demo_12_10.module.bo.respontGet;
import com.example.demo_12_10.module.in.userIn;
import org.springframework.stereotype.Service;

@Service
public interface userService {
    respontGet GetAllData() ;
    respont AddData(userIn userIn);
    respont DeleteData(Integer Id);
    respont UpdateData(Integer Id, userIn userIn);

}

