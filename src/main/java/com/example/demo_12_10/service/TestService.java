package com.example.demo_12_10.service;

import com.example.demo_12_10.module.bo.respont;
import com.example.demo_12_10.module.in.userIn;
import org.springframework.stereotype.Service;

@Service
public interface TestService  {
    respont get();

}
