package com.example.demo_12_10.service;

import com.example.demo_12_10.mapper.userMapper;
import com.example.demo_12_10.module.bo.respont;
import com.example.demo_12_10.module.bo.respontGet;
import com.example.demo_12_10.module.dto.userDto;
import com.example.demo_12_10.module.entity.userEntity;
import com.example.demo_12_10.module.in.userIn;
import com.example.demo_12_10.reponsitory.userReponsitory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
@Component
public class userImpl implements userService {
    @Autowired
    private userReponsitory userReponsitory;
    @Override
    public respontGet GetAllData() {

        List<userEntity> page1= userReponsitory.findAll();
        List<userDto>list=page1.stream().map(userMapper::mapDto).collect(Collectors.toList());
        return new respontGet("data ve ",list);
    }
    @Override
        public respont AddData(userIn userIn) {
                userEntity user = userMapper.map(userIn);
                if(userIn.getUserName()==null||userIn.getUserName()=="")
                {
                    return new respont("user name khong  the de trong");
                }
                user.setCreatedDate(LocalDate.now());
                userReponsitory.save(user);
                return new respont("add thanh cong",user);
        }
    @Override
    public respont DeleteData(Integer Id) {
         userEntity userEntity = userReponsitory.getById(Id);
         userReponsitory.delete(userEntity);
         List<userEntity>list= userReponsitory.findAll();
         List<userDto>listDto=list.stream().map(userMapper::mapDto).collect(Collectors.toList());
         return new respont("xoa thanh cong",listDto);
    }
    @Override
    public respont UpdateData(Integer Id, userIn userIn) {
        userEntity user = userReponsitory.getById(Id);
        if(user.getUserName()==null||userIn.getUserName()==""){
            user.setUserName(user.getUserName());
        }
        user.setPassWord(userIn.getPassWord());
        user.setStatus(userIn.getStatus());
        user.setDateOfBirth(userIn.getDateOfBirth());
        userReponsitory.save(user);
        return new respont("update thanh cong");
    }
}
