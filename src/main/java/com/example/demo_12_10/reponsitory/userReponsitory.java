package com.example.demo_12_10.reponsitory;

import com.example.demo_12_10.module.entity.userEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface userReponsitory extends JpaRepository<userEntity,Integer> {
    userEntity getById(Integer Id);

    Page<userEntity> findAllByUserNameContaining(Pageable pageable,String textSearch);

}
