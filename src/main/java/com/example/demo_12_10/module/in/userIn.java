package com.example.demo_12_10.module.in;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class userIn {
    private String userName;
    private String passWord;
    private Boolean status;
    private LocalDate createdDate;
    private String dateOfBirth;

    public userIn(String userName, String dateOfBirth) {
        this.userName = userName;
        this.dateOfBirth = dateOfBirth;
    }

    public userIn(String userName, String passWord, Boolean status, String dateOfBirth) {
        this.userName = userName;
        this.passWord = passWord;
        this.status = status;
        this.dateOfBirth = dateOfBirth;
    }
}
