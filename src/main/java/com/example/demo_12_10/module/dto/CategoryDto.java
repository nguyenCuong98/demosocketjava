package com.example.demo_12_10.module.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CategoryDto {
    private Integer id;
    private String maCp;
    private Integer thiGia;
    private Integer bienDo;
    private Integer klgd;
}
