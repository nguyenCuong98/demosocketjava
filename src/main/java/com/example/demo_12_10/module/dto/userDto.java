package com.example.demo_12_10.module.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class userDto {
    private Integer id;
    private String userName;
    private String passWord;
    private Boolean status;
    private LocalDate createdDate;
    private String dateOfBirth;

}
