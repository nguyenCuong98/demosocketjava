package com.example.demo_12_10.module.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class respont {
    public  String message;
    public Object data;
    public  Object list;


    public respont(String message, Object data) {
        this.message = message;
        this.data = data;
    }

    public respont(String message, Object data, Object list) {
        this.message = message;
        this.data = data;
        this.list = list;
    }

    public respont(String message ) {
        this.message = message;

    }
}

