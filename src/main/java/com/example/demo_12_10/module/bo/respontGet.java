package com.example.demo_12_10.module.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class respontGet {
    private String messenger ;
    private Object data;
    private Number totalPage;
    private  Number totalElement;
    private String textSearch;

    public respontGet(String messenger, Object data) {
        this.messenger = messenger;
        this.data = data;
    }
}
